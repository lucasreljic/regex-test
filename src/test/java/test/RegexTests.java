package test;
//package com.gbe.ice.message;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
//import sun.jvm.hotspot.HelloWorld;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import com.fazecast.jSerialComm.*;

import javax.swing.filechooser.FileView;

import org.junit.Test;
import org.w3c.dom.Text;

public class RegexTests {
	private List<String> actualStrings = new ArrayList<String>();
	//private Scan scan = new Scan();
	private String sfeed = "";
		//String received = "";
		String message = "{status: Running, ballpos: -0.0375, distance: 1.243112}{status: Running, ballpos: -0.0375, distance: 1.243112}{status: Running, ballpos: -0.0375, distance: 1.243112}";//send this string
		static SerialPort comPort = SerialPort.getCommPort("COM3");
	public static void main(String[] args) {    
		
	}
	public String getScan() throws FileNotFoundException{
        File file = new File("C:\\Users\\Lucas\\OneDrive\\Desktop\\regextest.txt");
		//System.out.println(file.exists());
		Scanner scan = new Scanner(file);
		String feed = "";
        while (scan.hasNextLine()){
			feed = feed.concat(scan.nextLine());
		}   
        return feed;
	}
	
	@Test
	public void testLucasRegex() throws FileNotFoundException {
		//read/write test
		//read
		comPort.openPort();
		try {
			Thread.sleep(1000);
			
			byte[] writeBuffer = message.getBytes();
			comPort.writeBytes(writeBuffer, writeBuffer.length);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		comPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, 0, 0);
		InputStream in = comPort.getInputStream();
		String output = "";
		try
		{
		for (int j = 0; j < 80; j++){
			if(in.available()>0){
				char c = (char)in.read();
				output += c;
				System.out.print(c);
			}
				
		}
			
			//assertEquals(message, (char)in.read());
			//assertEquals(message, (char)in.read());
		in.close();
		} catch (Exception e) { e.printStackTrace(); }
				
		comPort.closePort();
		
		//scanner.nextLine();
		
		
		
		//String expectedStringToFind []= {"{status: Running, ballpos: -0.0375, distance: 1.143484}", "{status: Running, ballpos: -0.0375, distance: 1.243484}", "{status: Running, ballpos: -0.1275, distance: 1.243112}"}; 
		sfeed = output;
		//System.out.println(getScan());
		long startTime = System.currentTimeMillis();// Regex = \{status: [\w]{1,3}[\s]?[\w]{5,10}, (?:ball|strip)pos: [\D]?[\d]\.?[\d]*, distance: [\D]?[\d]\.?[\d]*\}
		Pattern pat = Pattern.compile("\\{status: [\\w]{1,3}[\\s]?[\\w]{5,10}, (?:ball|strip)pos: [\\D]?0\\.[\\d]*, distance: [\\D]?[\\d]{1,2}\\.?[\\d]*\\}" );//add stx and etx
		Matcher matcher = pat.matcher(sfeed);//https://regex101.com/r/y6i7n9/1
		matcher.toMatchResult();
	    
		while (matcher.find()){
			System.out.println(matcher.group());
			actualStrings.add(matcher.group());
			//actualStrings[i] = matcher.group();
		}
		
		//assertTrue("Not enough Strings", actualStrings.size() > 1);
		System.out.println("String Count: " +  actualStrings.size());
		//assertEquals(expectedStringToFind, matcher.group());
		//assertArrayEquals(expectedStringToFind, actualStrings);
		long endTime = System.currentTimeMillis();
	    long totalTime = endTime-startTime;
	    assertTrue(totalTime<10);
	    System.out.println("Time to run (in ms): "  + totalTime);
    }
    
   
}